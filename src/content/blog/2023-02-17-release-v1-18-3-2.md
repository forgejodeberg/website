---
title: Forgejo Security Release 1.18.3-2
publishDate: 2023-02-17
tags: ['releases', 'security']
release: 1.18.3-2
excerpt: Forgejo v1.18.3-2 stable release update fixes CVE-2023-22490 and CVE-2023-23946 and bug fixes
---

Today [Forgejo v1.18.3-2](https://codeberg.org/forgejo/forgejo/releases/tag/v1.18.3-2) was released.

This release contains a security fix for Forgejo container images, as described below. When Forgejo runs from a binary, recommendations to upgrade the `git` version installed alongside it are also provided.

This release also contains bug fixes as detailed [in the release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-18-3-2).

### Recommended Action

We recommend that all installations running a version affected by the issues described below are upgraded to the latest version as soon as possible.

- When using a Forgejo binary: upgrade the `git` package to a version greater or equal to v2.39.2, v2.38.4, v2.37.6, v2.36.5, v2.35.7, v2.34.7, v2.33.7, v2.32.6, v2.31.7 or v2.30.8
- When using a Forgejo container image: `docker pull codeberg.org/forgejo/forgejo:1.18.3-2`

### Security issues in Git

Git [recently announced](https://github.blog/2023-02-14-git-security-vulnerabilities-announced-3/) new versions to address two CVEs ([CVE-2023-22490](https://cve.circl.lu/cve/CVE-2023-22490), [CVE-2023-23946](https://cve.circl.lu/cve/CVE-2023-23946)). On 14 February 2023, Git published the maintenance release v2.39.2, together with releases for older maintenance tracks v2.38.4, v2.37.6, v2.36.5, v2.35.7, v2.34.7, v2.33.7, v2.32.6, v2.31.7, and v2.30.8.

The [Forgejo security team](/.well-known/security.txt) analyzed both CVE and concluded that they **cannot be exploited via Forgejo**. It is however recommended to upgrade `git` as a precaution.

### Fixing Git when using a Forgejo binary

When installed as a binary [downloaded from the Forjego releases](https://codeberg.org/forgejo/forgejo/releases/tag/v1.18.3-2) repository, it is the responsibility of the Forgejo admin to install `git` independently. Upgrading to a patched `git` package (with a version greater or equal to v2.39.2, v2.38.4, v2.37.6, v2.36.5, v2.35.7, v2.34.7, v2.33.7, v2.32.6, v2.31.7 or v2.30.8) is therefore enough to fix the problem, even if Forgejo is not upgraded.

### Fixing Git when using a Forgejo container image

When installed as an image [downloaded from the Forgejo registry](https://codeberg.org/forgejo/-/packages/container/forgejo/versions), the container includes both the Forgejo binary and the `git` binary, as obtained from [Alpine 3.16](https://pkgs.alpinelinux.org/packages?name=git&branch=v3.16). `Forgejo 1.18.3-1` contains a vulnerable `git` binary:

```
$ docker run --rm codeberg.org/forgejo/forgejo:1.18.3-1 git --version
git version 2.36.4
```

The [Forgejo 1.18.3-2 images](https://codeberg.org/forgejo/-/packages/container/forgejo/versions) were built shortly after the patched `git` binary was upgraded in `Alpine 3.16` and is not vulnerable:

```
$ docker run --rm codeberg.org/forgejo/forgejo:1.18.3-2 git --version
git version 2.36.5
```

In this case it is necessary to upgrade Forgejo to `1.18.3-2` to get the fixed `git` binary. The [rootless](https://codeberg.org/forgejo/-/packages/container/forgejo/1.18-rootless) variant of Forgejo also includes the `git` binary and can be upgraded in the same way.

### Forgejo installation instructions

See the [download page](/download)
for instructions for installation instructions. If you are upgrading from `Forgejo 1.18.3-1` (or `Gitea 1.18`) no manual action is required. If you're on `Gitea v1.17.x` or older please read the [release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#1-18-0-1)
carefully, and in particular check out the [breaking changes](https://blog.gitea.io/2022/12/gitea-1.18.0-is-released/#breaking-changes) section of Gitea's blog post.

The actual upgrade process is as simple as replacing the Gitea binary or container image
with the corresponding [Forgejo binary](https://codeberg.org/forgejo/forgejo/releases/tag/v1.18.3-2)
or [container image](https://codeberg.org/forgejo/-/packages/container/forgejo/1.18.3-2).
If you're using the container images, you can use the
[`1.18`](https://codeberg.org/forgejo/-/packages/container/forgejo/1.18) tag
to stay up to date with the latest `1.18.x` point release automatically.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo, we'd love to hear from you!
Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports. You can also find us [on the Fediverse](https://floss.social/@forgejo),
or drop by [our Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) to say hi!
